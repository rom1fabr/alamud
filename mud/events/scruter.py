# -*- coding: utf-8 -*-
# Copyright (C) 2021 Groupe Frederic, Romain, Valentin, IUT d'Orléans
#==============================================================================

from .event import Event3

class ScruterEvent(Event3):
    NAME = "scruter"
    def perform(self):
        if not self.object.has_prop("scrutable"):
            self.fail()
        self.inform("scruter")