# -*- coding: utf-8 -*-
# Copyright (C) 2021 Romain, Frederic et Valentin Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class CallEvent(Event3):
    NAME = "call"

    def perform(self):
        if not self.object.has_prop("callable"):
            self.fail()
            return self.inform("call.failed")
        self.inform("call")
