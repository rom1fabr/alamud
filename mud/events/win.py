# -*- coding: utf-8 -*-
# Copyright (C) 2021 Frederic, Romain, Valentin, IUT d'Orléans
#==============================================================================

from .event import Event1

class WinEvent(Event1):
    NAME = "win"
    def perform(self):
        self.inform("win")
        cont = self.actor.container()
        for x in list(self.actor.contents()):
            x.move_to(cont)
        self.actor.move_to(None)
