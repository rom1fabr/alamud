# -*- coding: utf-8 -*-
# Copyright (C) 2021 Groupe Frederic, Romain, Valentin, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class JumpEvent(Event2):
    NAME = "jump"
    def perform(self):
        self.inform("jump")


class JumpWithEvent(Event3):
    NAME = "jump-with"
    def perform(self):
        if not self.object.has_prop("jumpable"):
            self.fail()
        self.inform("jump-with")