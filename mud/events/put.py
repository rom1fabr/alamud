# -*- coding: utf-8 -*-
# Copyright (C) 2021 Romain, Frederic et Valentin Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class PutEvent(Event2):
    NAME = "put"

    def perform(self):
        if not self.object.has_prop("putable"):
            self.fail()
            return self.inform("put.failed")
        self.inform("put")