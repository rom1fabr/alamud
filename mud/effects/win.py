# -*- coding: utf-8 -*-
# Copyright (C) 2021 Frederic, Romain, Valentin, IUT d'Orléans
#==============================================================================

from .effect import Effect1
from mud.events.win import WinEvent

class WinEffect(Effect1):
    EVENT = WinEvent