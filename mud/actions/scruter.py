# -*- coding: utf-8 -*-
# Copyright (C) 2021 Frederic, Romain, Valentin, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import ScruterEvent

class ScruterAction(Action3):
    EVENT = ScruterEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "scruter"