# -*- coding: utf-8 -*-
# Copyright (C) 2021 Frederic, Romain, Valentin, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import JumpEvent, JumpWithEvent

class JumpAction(Action2):
    EVENT = JumpEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "jump"

class JumpWithAction(Action3):
    EVENT = JumpWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "jump-with"