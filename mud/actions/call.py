# -*- coding: utf-8 -*-
# Copyright (C) 2021 Romain, Frederic et Valentin Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import CallEvent

class CallAction(Action3):
    EVENT = CallEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "call"
    
